<?php get_header(); ?>

	<section class="page-header">
		<div class="wrapper">
			
			<div class="headline">
				<h4><a href="<?php echo site_url('/market-updates/'); ?>">Market Updates</a> / Blog</h4>
			</div>

		</div>
	</section>

	<?php
		get_template_part('partials/newsletter-sign-up');
		$posts_page = get_option('page_for_posts');
		get_template_part('partials/cat-nav');
	?>

	<section class="featured-post">

		<?php

			$fps = get_field('featured_post', $posts_page); if( $fps ): ?>
				   
			<?php foreach( $fps as $fp): ?>

				<div class="photo">
					<div class="content">
						<a href="<?php echo get_permalink($fp->ID); ?>">
							<img src="<?php $image = get_field('hero_image', $fp->ID); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
						</a>
					</div>
				</div>

				<div class="info">
					<div class="wrapper">
						<div class="headline">
							<h3><a href="<?php echo get_permalink($fp->ID); ?>"><?php echo get_the_title($fp->ID); ?></a></h3>
						</div>

						<div class="meta">
							<p><?php the_time('n/j/y'); ?>: <a href="<?php $author = get_field('author', $fp->ID); echo get_permalink($author->ID); ?>"><?php echo get_the_title($author->ID); ?></a></p>
						</div>	

						<div class="cta">
							<a href="<?php echo get_permalink($fp->ID); ?>" class="btn clear-white">Read More</a>
						</div>				
					</div>
				</div>

		    <?php endforeach; ?>
		<?php endif; ?>		

	</section>


	<section class="blog-posts">
		<div class="wrapper">
		
			<?php if ( have_posts() ): ?>

				<div class="posts">

					<?php $col = 1; $i =1; $row = 1; while ( have_posts() ): the_post(); ?>

						<article class="grid-col-<?php echo $col; ?> grid-row-<?php echo $row; ?>">
							<div class="photo">
								<div class="content">
									<a href="<?php the_permalink(); ?>">
										<img src="<?php $image = get_field('hero_image'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
									</a>
								</div>
							</div>

							<div class="info">
								<div class="headline">
									<h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
								</div>

								<div class="meta">
									<p><?php the_time('n/j/y'); ?>: <a href="<?php $author = get_field('author'); echo get_permalink($author->ID); ?>"><?php echo get_the_title($author->ID); ?></a> | 	<?php $categories = get_the_category(); if ( ! empty( $categories ) ): ?><a href="<?php echo get_category_link($categories[0]->term_id); ?>"><?php echo $categories[0]->name; ?></a><?php endif; ?></p>
								</div>	

								<div class="cta">
									<a href="<?php the_permalink(); ?>" class="btn charcoal">Read More</a>
								</div>	
							</div>
							
						</article>

						<?php
							 if($col == 2) {
							 	$col = 1;
							 } else {
							 	$col++;
							 }

							 if($i % 2 == 0){
							 	$row++;
							 }
						?>

					<?php $i++; endwhile; ?>

				</div>

			<?php endif; ?>

<?php the_posts_pagination( array(
	'mid_size'  => 2,
	'prev_text' => __(''),
	'next_text' => __(''),
) );

?>
		</div>
	</section>
	
<?php get_footer(); ?>