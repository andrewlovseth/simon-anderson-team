<?php get_header(); ?>

	<?php if ( have_posts() ): while ( have_posts() ): the_post(); ?>

		<section class="page-header">
			<div class="wrapper">
				
				<div class="headline">
					<h4><a href="<?php echo site_url('/team/'); ?>">Team</a> / <?php the_title(); ?></h4>
				</div>

			</div>
		</section>

		<article>
			<div class="featured-image" >
				<div class="content">
					<img src="<?php $image = get_field('hero_image'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
				</div>
			</div>

			<div class="article-header">
				<div class="wrapper">

					<div class="headline">
						<h2><?php the_title(); ?></h2>
					</div>
						
					<?php if(get_field('title')): ?>
						<div class="row title copy p2">
							<p><?php the_field('title'); ?></p>
						</div>
					<?php endif; ?>

					<?php if(get_field('direct_phone')): ?>
						<div class="row direct copy p2">
							<p>Direct: <?php the_field('direct_phone'); ?></p>
						</div>
					<?php endif; ?>

					<?php if(get_field('mobile_phone')): ?>
						<div class="row mobile copy p2">
							<p>Mobile: <?php the_field('mobile_phone'); ?></p>
						</div>
					<?php endif; ?>

					<?php if(get_field('email')): ?>
						<div class="row email copy p2">
							<p><a href="mailto:<?php the_field('email'); ?>"><?php the_field('email'); ?></a></p>
						</div>	
					<?php endif; ?>	
					
				</div>			
			</div>

			<div class="article-body copy p2">
				<div class="wrapper">

					<?php the_field('long_bio'); ?>

				</div>
			</div>
		</article>

	<?php endwhile; endif; ?>

	<?php get_template_part('partials/agent-modal'); ?>
	
<?php get_footer(); ?>