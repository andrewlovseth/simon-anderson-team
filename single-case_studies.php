<?php


get_header(); ?>

	<?php if ( have_posts() ): while ( have_posts() ): the_post(); ?>

		<section class="page-header" <?php get_template_part('partials/animations/fade-up'); ?>>
			<div class="wrapper">
				
				<div class="headline">
					<h4><a href="<?php echo site_url('/why-us/'); ?>">Why Work with Us</a> / <?php the_title(); ?></h4>
				</div>

			</div>
		</section>

		<section class="hero cover" style="background-image: url(<?php $image = get_field('hero_image'); echo $image['url']; ?>);" <?php get_template_part('partials/animations/fade-up'); ?>>
			<div class="content">
				<div class="wrapper">

					<div class="info">
						<div class="status">
							<?php
								$status = get_field('status');
								$label = preg_replace("/[\-_]/", " ", $status);
								$label = substr($label, 1);
							?>

							<img class="icon" src="<?php bloginfo('template_directory') ?>/images/status/<?php echo $status; ?>_dark.svg" alt="">
							<span class="label"><?php echo $label; ?></span>
						</div>

						<div class="headline">
							<h1><?php the_title(); ?></h1>
						</div>
					</div>

				</div>
			</div>
		</section>

		<section class="status">
			<div class="wrapper">
				
				<div class="logo" <?php get_template_part('partials/animations/fade-right'); ?>>
					<img src="<?php $image = get_field('logo'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
				</div>

				<div class="info" <?php get_template_part('partials/animations/fade-left'); ?>>
					<div class="headline">
						<h3><?php the_field('status_headline'); ?></h3>
					</div>

					<div class="copy p2">
						<?php the_field('status_copy'); ?>
					</div>
				</div>

			</div>
		</section>

		<section class="summary">
			<div class="wrapper">

				<div class="info" <?php get_template_part('partials/animations/fade-right'); ?>>
					<div class="headline">
						<h2><?php the_field('summary_headline'); ?></h2>
					</div>

					<div class="copy p2">
						<?php the_field('summary_copy'); ?>
					</div>
				</div>
				

				<div class="site-details details-table" <?php get_template_part('partials/animations/fade-left'); ?>>
					<div class="headline">
						<h3>Site Details</h3>
					</div>

					<div class="table">
						<?php if(have_rows('site_details')): while(have_rows('site_details')): the_row(); ?>
							 
						    <div class="row">
						    	<div class="key">
						    		<span><?php the_sub_field('key'); ?></span>
						    	</div>

						    	<div class="value">
						    		<span><?php the_sub_field('value'); ?></span>
						    	</div>
						    </div>

						<?php endwhile; endif; ?>
					</div>

					<div class="cta">
						<?php 

						$link = get_field('original_listing');

						if( $link ): 
							$link_url = $link['url'];
							$link_title = $link['title'];
							?>
							<a class="btn yellow" href="<?php echo esc_url($link_url); ?>">View Original Listing</a>
						<?php endif; ?>
					</div>
				</div>

			</div>
		</section>

		<section class="testimonials">
			<div class="wrapper">

				<div class="testimonials-wrapper">
					<?php if(have_rows('testimonials')): while(have_rows('testimonials')): the_row(); ?>

					    <div class="testimonial">
					    	<div class="quote">
					    		<p><?php the_sub_field('quote'); ?></p>
					    	</div>

					    	<div class="source">
					    		<p><?php the_sub_field('source'); ?></p>
					    	</div>
					        
					    </div>

					<?php endwhile; endif; ?>
				</div>			

			</div>
		</section>
		

	<?php endwhile; endif; ?>	

<?php get_footer(); ?>