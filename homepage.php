<?php

/*

	Template Name: Home

*/

get_template_part('template-parts/header/template-options');

get_header(); ?>

	<section class="hero cover" style="background-image: url(<?php $image = get_field('hero_image'); echo $image['url']; ?>);">
		<div class="content">
			<div class="wrapper">


				<div class="info">
					<div class="headline">
						<h1><?php the_field('hero_headline'); ?></h1>
					</div>

					<div class="copy p1">
						<p><?php the_field('hero_sub_headline'); ?></p>	
					</div>

					<div class="links">
						<?php if(have_rows('hero_links')): while(have_rows('hero_links')): the_row(); ?>
						 
						    <div class="link">

								<?php 

								$link = get_sub_field('link');

								if( $link ): 
									$link_url = $link['url'];
									$link_title = $link['title'];
									?>
									<a class="btn <?php the_sub_field('color'); ?>" href="<?php echo esc_url($link_url); ?>"><?php echo esc_html($link_title); ?></a>
								<?php endif; ?>


						    </div>

						<?php endwhile; endif; ?>						

					</div>
				</div>		


			</div>
		</div>
	</section>

	
	<section id="valuation">
		<div class="section-header" <?php get_template_part('partials/animations/fade-up'); ?>>
			<div class="wrapper">
				<h4>Apartment Valuation</h4>
			</div>
		</div>

		<div class="valuation-form">
			<div class="wrapper">
				<div class="headline" <?php get_template_part('partials/animations/fade-right'); ?>>
					<h2><?php the_field('valuation_headline'); ?></h2>
				</div>

				<div class="form" <?php get_template_part('partials/animations/fade-left'); ?>>
					<div class="copy p2">
						<p><?php the_field('valuation_form_description'); ?></p>
					</div>

					<?php echo do_shortcode('[wpforms id="512"]	'); ?>					
				</div>
			</div>		
		</div>
	</section>


	<section class="featured-listings">
		<div class="wrapper">
			
			<div class="section-header center" <?php get_template_part('partials/animations/fade-up'); ?>>
				<h4>Featured Listings</h4>
			</div>

			<div class="listings" <?php get_template_part('partials/animations/fade-up'); ?>>
				<?php $posts = get_field('featured_listings'); if( $posts ): ?>
				    <?php foreach( $posts as $post): setup_postdata($post); ?>
				        <?php get_template_part('partials/listing'); ?>
				    <?php endforeach;  wp_reset_postdata(); ?>
				<?php endif; ?>
			</div>

			<div class="view-all" <?php get_template_part('partials/animations/fade-up'); ?>>
				<a href="<?php echo site_url('/listings/'); ?>" class="btn yellow">View all listings</a>
			</div>

		</div>
	</section>


	<section class="why-us cover" style="background-image: url(<?php $image = get_field('why_work_with_us_background_image'); echo $image['url']; ?>);">
		<div class="content">
			<div class="wrapper">
				
				<div class="headline" <?php get_template_part('partials/animations/fade-left'); ?>>
					<h2><?php the_field('why_work_with_us_headline'); ?></h2>
				</div>

				<div class="copy p1" <?php get_template_part('partials/animations/fade-left'); ?>>
					<p><?php the_field('why_work_with_us_deck'); ?></p>
				</div>

				<div class="cta" <?php get_template_part('partials/animations/fade-left'); ?>>
					<?php 

					$link = get_field('why_work_with_us_cta');

					if( $link ): 
						$link_url = $link['url'];
						$link_title = $link['title'];
						?>
						<a class="btn clear-white" href="<?php echo esc_url($link_url); ?>"><?php echo esc_html($link_title); ?></a>
					<?php endif; ?>
				</div>

			</div>			
		</div>
	</section>


	<section class="stats">
		<div class="wrapper">

			<?php if(have_rows('stats')): while(have_rows('stats')): the_row(); ?>

			    <div class="stat">
			    	<div class="headline">
				    	<h2>
				    		<span class="prefix"><?php the_sub_field('prefix'); ?></span><span class="counter" data-count="<?php the_sub_field('ending_number'); ?>"><?php the_sub_field('starting_number'); ?></span><span class="unit"><?php the_sub_field('unit'); ?></span>
				    	</h2>
			    	</div>

			    	<div class="copy p2">
			    		<p><?php the_sub_field('description'); ?></p>	
			    	</div>			    				        
			    </div>

			<?php endwhile; endif; ?>		

		</div>
	</section>


	<section class="services cover" style="background-image: url(<?php $image = get_field('services_background_image'); echo $image['url']; ?>);">
		<div class="content">
			<div class="wrapper">

				<div class="icons" <?php get_template_part('partials/animations/fade-left'); ?>>
					<?php if(have_rows('services_icons')): while(have_rows('services_icons')): the_row(); ?>
					    <div class="service">
					    	<div class="icon">
					    		<img src="<?php $image = get_sub_field('icon'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
					    	</div>

					    	<span class="description">
					    		<?php the_sub_field('description'); ?>
					    	</span>				        
					    </div>
					<?php endwhile; endif; ?>
				</div>

				<div class="headline" <?php get_template_part('partials/animations/fade-left'); ?>>
					<h2><?php the_field('services_headline'); ?></h2>
				</div>				

				<div class="copy p1" <?php get_template_part('partials/animations/fade-left'); ?>>
					<p><?php the_field('services_sub_headline'); ?></p>
				</div>

				<div class="cta" <?php get_template_part('partials/animations/fade-left'); ?>>
					<?php 

					$link = get_field('services_cta');

					if( $link ): 
						$link_url = $link['url'];
						$link_title = $link['title'];
						?>
						<a class="btn clear-white" href="<?php echo esc_url($link_url); ?>"><?php echo esc_html($link_title); ?></a>
					<?php endif; ?>
				</div>

			</div>
		</div>
	</section>


	<?php get_template_part('partials/testimonials'); ?>


	<section class="market-updates">
		<div class="wrapper">

			<div class="section-header" <?php get_template_part('partials/animations/fade-up'); ?>>
				<h4><?php the_field('market_updates_headline'); ?></h4>
			</div>

			<div class="posts">
				<?php $posts = get_field('market_updates'); if( $posts ): ?>
				    <?php foreach( $posts as $post): setup_postdata($post); ?>

				    	<?php if($post->post_type == 'research'): ?>

				    		<?php get_template_part('partials/research-preview'); ?>

				    	<?php else: ?>

				    		<?php get_template_part('partials/post-preview'); ?>

				    	<?php endif; ?>

				    <?php endforeach;  wp_reset_postdata(); ?>
				<?php endif; ?>		
			</div>

		</div>
	</section>

<?php get_footer(); ?>