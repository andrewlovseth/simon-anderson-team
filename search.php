<?php

$search_query = get_search_query();

get_header(); ?>

	<section class="page-header" <?php get_template_part('partials/animations/fade-up'); ?>>
		<div class="wrapper">
			
			<div class="headline">
				<h4>Search Results: <?php echo $search_query; ?></h4>
			</div>

		</div>
	</section>	

	<section class="results" <?php get_template_part('partials/animations/fade-up'); ?>>
		<div class="wrapper">
		
			<?php if ( have_posts() ): ?>

				<?php $types = array('listings', 'case_studies', 'post', 'research', 'page', 'agents'); foreach( $types as $type ): ?>

					<?php while ( have_posts() ): the_post(); ?>

						<?php if( $type == get_post_type() ): ?>

							<?php get_template_part('partials/search/content', $type); ?>

						<?php endif; ?>
        
				    <?php endwhile; rewind_posts();  ?>

				<?php endforeach; ?>

			<?php endif; ?>

			<?php the_posts_pagination( array(
				'mid_size'  => 2,
				'prev_text' => __(''),
				'next_text' => __(''),
			) ); ?>

		</div>
	</section>
	
<?php get_footer(); ?>