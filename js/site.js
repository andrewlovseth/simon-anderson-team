(function ($, window, document, undefined) {

	$(document).ready(function() {

		// rel="external"
		$('a[rel="external"]').click( function() {
			window.open( $(this).attr('href') );
			return false;
		});


		// Menu Toggle
		$('#toggle').click(function(){
			$('body').toggleClass('nav-open');
			return false;
		});


		// Search Trigger
		$('.search-trigger').click(function(){
			$('body').toggleClass('search-open');
			return false;
		});


		// Search Close
		$('.search-close').click(function(){
			$('body').toggleClass('search-open');
			return false;
		});


		// Testimonials
		$('.testimonials-wrapper').slick({
			arrows: true,
			dots: true,
	  		slidesToShow: 1,
			autoplay: true,
			autoplaySpeed: 6000,
		});




		// Details Gallery
		$('.details-gallery .gallery').slick({
			arrows: true,
			dots: true,
	  		slidesToShow: 1,
			autoplay: true,
			autoplaySpeed: 6000,
		});

		// Smooth Scroll to Menu Section
		$('.anchor a').smoothScroll({offset: -108});

		$('.anchor a').on('click', function(){
			$('.anchor a').closest('.anchor').removeClass('active');
			$(this).closest('.anchor').addClass('active');

			return false;
		});

		$('.question').click(function(){
			$(this).toggleClass('open');

			var answer = $(this).siblings('.answer');
			$(answer).slideToggle(300);

			return false;
		});



		// Case Studies Carousel
		$('section.case-studies').slick({
			arrows: true,
			dots: true,
	  		slidesToShow: 1,
			autoplay: true,
			autoplaySpeed: 6000,
			pauseOnFocus: true,
			pauseOnHover: true
		});


		// Blog Video
		$('.photo.video').fitVids();
			

		// Blog Video
		$('.article-body .wrapper').fitVids();

		$('a.smooth').smoothScroll({offset: -108});
			
	});


	$(document).on("click", '#agent-modal .close', function(event) { 

		$('#overlay').removeClass('show');			
		return false;

	});


	var headerHeight = $('header').outerHeight(),
		processNavHeight = $('.processes-nav').outerHeight(),
		doubleNavHeight = headerHeight + processNavHeight;

	// Sticky Nav
	$(window).scroll(function() {    
	    var scroll = $(window).scrollTop();

	    var search_icon = $('#search-icon-white'),
	    	search_icon_dark = $(search_icon).data('dark'),
	    	search_icon_white = $(search_icon).data('white');
		
		if (scroll >= headerHeight) {
			$('body').addClass('sticky');
			$(search_icon).attr('src', search_icon_dark);

		} else {
			$('body').removeClass('sticky');
			$(search_icon).attr('src', search_icon_white);
		}

		$('.processes-nav .anchor').each(function () {
			var anchor = $(this),
				section = $(anchor).find('a').attr("href"),
				sectionTop = $(section).offset().top,
				sectionHeight = $(section).outerHeight(),
				scrollOffset = scroll + doubleNavHeight;

	        if (sectionTop <= scrollOffset && sectionTop + sectionHeight > scrollOffset) {
	            $('.processes-nav .anchor').removeClass('active');
	            anchor.addClass("active");
	        }		
		});
	}); 



	// Stats
	if ( window.location.pathname == '/' ){

		var isInViewport = function (elem) {
		    var bounding = elem.getBoundingClientRect();
		    return (
		        bounding.top >= 0 &&
		        bounding.left >= 0 &&
		        bounding.bottom <= (window.innerHeight || document.documentElement.clientHeight) &&
		        bounding.right <= (window.innerWidth || document.documentElement.clientWidth)
		    );
		};

		var stats = document.querySelector('.stats');

		window.addEventListener('scroll', function (event) {
			if (isInViewport(stats)) {

				$('.counter').each(function() {
					var $this = $(this),
						countTo = $this.attr('data-count');

					$({ countNum: $this.text()}).animate({
							countNum: countTo
						}, {
							duration: 800,
							easing:'linear',
							step: function() {
							$this.text(Math.floor(this.countNum));
						},
							complete: function() {
							$this.text(this.countNum);
						}
					});    
				});
			}
		});
	}


	 $( window ).on( "load", function() {
		AOS.init();
	});


})(jQuery, window, document);
