(function ($, window, document, undefined) {

	// Load posts on page load
	get_listings();

	// Get Selected Property Types
	function getSelectedPropertyTypes() {
		var property_types = new Array();

		$('.filters .property-type .toggle a').each( function(i){
			if($(this).hasClass('on')) {
				property_types[i] = $(this).data('filter');
			}			
		});

		return property_types;
	}

	// Get Status
	function getPropertyStatus() {
		var property_status = $('.page-template-listings-status .listings').data('status');
		return property_status;
	}



	// On select filter change
	$('.filters .toggle a').on('click', function(){
		$(this).toggleClass('on');

		get_listings();
	});

	
	//Main ajax function
	function get_listings() {
		var ajax_url = ajax_listing_params.ajax_url;

		$.ajax({
			type: 'GET',
			url: ajax_url,
			data: {
				action: 'listings_filter',
				property_types: getSelectedPropertyTypes,
				property_status: getPropertyStatus,

			},
			beforeSend: function () {
				$('.listings-main #response').html();
			},
			success: function(data) {
				$('.listings-main #response').html(data);
			},
			error: function() {
				$(".listings-main #response").html('<p>There has been an error</p>');
			}
		}).done(function() {

			$('.agent-trigger').on('click', function() {
				$('#overlay-wrapper').empty();

				var agent_url = $(this).data('agent');
				$('#overlay-wrapper').load(agent_url + ' #agent-modal');
				$('#overlay').addClass('show');
				return false;
			});

			
		});			
	}
	
})(jQuery, window, document);