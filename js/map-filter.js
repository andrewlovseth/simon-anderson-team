(function ($, window, document, undefined) {

	// Load posts on page load
	get_map_listings();

	// Get Selected Property Types
	function getSelectedPropertyTypes() {
		var property_types = new Array();

		$('.filters .property-type .toggle a:not(.off)').each( function(i){
			property_types[i] = $(this).data('filter');
		});

		return property_types;
	}

	// On select filter change
	$('.filters .toggle a').on('click', function(){
		get_map_listings();
	});
	
	//Main ajax function
	function get_map_listings() {
		var ajax_url = ajax_map_listings_params.ajax_url;

		$.ajax({
			type: 'GET',
			url: ajax_url,
			data: {
				action: 'map_listings_filter',
				property_types: getSelectedPropertyTypes
			},
			beforeSend: function () {
				$('#map-view #response').html();
			},
			success: function(data) {
				//Hide loader here
				$('#map-view #response').html(data);
			},
			error: function() {
				$("#map-view #response").html('<p>There has been an error</p>');
			}
		});				
	}	
	
})(jQuery, window, document);