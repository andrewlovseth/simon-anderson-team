<?php 
	global $theme;
	global $hero;
	$header_class = "";

	if($theme == "light") {
		$header_class .= "light ";
	}

	if($hero == "cover") {
		$header_class .= "nav-overlay ";
	}
?>

<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">

	<?php wp_head(); ?>

</head>

<?php if($header_class != ""): ?>
<body <?php body_class('has-hero'); ?>>
<?php else: ?>
<body <?php body_class(); ?>>
<?php endif; ?>

<?php wp_body_open(); ?>

	<header<?php if($header_class != ""): ?> class="<?php echo $header_class; ?>"<?php endif; ?>>
		<div class="wrapper">

			<?php get_template_part('template-parts/header/logo'); ?>

			<?php get_template_part('template-parts/header/navigation'); ?>

			<?php get_template_part('template-parts/header/search'); ?>

		</div>
	</header>