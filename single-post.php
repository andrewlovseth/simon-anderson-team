<?php get_header(); ?>

	<?php if ( have_posts() ): while ( have_posts() ): the_post(); ?>

		<section class="page-header" <?php get_template_part('partials/animations/fade-up'); ?>>
			<div class="wrapper">
				
				<div class="headline">
					<h4><a href="<?php echo site_url('/market-updates/'); ?>">Market Updates</a> / <a href="<?php echo site_url('/blog/'); ?>">Blog</a> / <?php the_title(); ?></h4>
				</div>

			</div>
		</section>

		<article <?php get_template_part('partials/animations/fade-up'); ?>>
			<div class="featured-image" >
				<div class="content">
					<img src="<?php $image = get_field('hero_image'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
				</div>
			</div>

			<div class="sharing">
				<div class="wrapper">

					<div class="link email">
						<a href="mailto:?subject=Seattle Multifamily Team Blog: <?php echo get_the_title(); ?>&body=<?php echo get_the_title(); ?>%0D%0A<?php echo get_permalink(); ?>">
							<img src="<?php bloginfo('template_directory') ?>/images/share-email-icon.svg" alt="Email">
						</a>
					</div>

					<div class="link twitter">
						<a href="https://twitter.com/intent/tweet/?text=<?php echo get_the_title(); ?>+<?php echo get_permalink(); ?>" rel="external">
							<img src="<?php bloginfo('template_directory') ?>/images/share-twitter-icon.svg" alt="Twitter">
						</a>
					</div>

					<div class="link facebook">
						<a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo get_permalink(); ?>" rel="external">
							<img src="<?php bloginfo('template_directory') ?>/images/share-facebook-icon.svg" alt="Facebook">
						</a>
					</div>

					<div class="link linkedin">
						<a href="https://www.linkedin.com/shareArticle?mini=true&url=<?php echo get_permalink(); ?>" rel="external">
							<img src="<?php bloginfo('template_directory') ?>/images/share-linkedin-icon.svg" alt="LinkedIn">
						</a>
					</div>

				</div>
			</div>

			<div class="article-header">
				<div class="wrapper">

					<div class="meta">
						<p><?php the_time('n/j/y'); ?>: <a href="<?php $author = get_field('author'); echo get_permalink($author->ID); ?>"><?php echo get_the_title($author->ID); ?></a> | 	<?php $categories = get_the_category(); if ( ! empty( $categories ) ): ?><a href="<?php echo get_category_link($categories[0]->term_id); ?>"><?php echo $categories[0]->name; ?></a><?php endif; ?></p>
					</div>
					<div class="headline">
						<h2><?php the_title(); ?></h2>
					</div>
					
				</div>			
			</div>

			<div class="article-body copy p2">
				<div class="wrapper">
					<?php the_content(); ?>
				</div>
			</div>
		</article>

		<section class="posts-pagination">
			<div class="wrapper">
				
				<div class="prev">
					<?php previous_post_link('%link', 'Previous'); ?>
				</div>	

				<div class="all">
					<a href="<?php echo site_url('/blog'); ?>">All Blog Posts</a>
					
				</div>

				<div class="next">
					<?php next_post_link('%link', 'Next'); ?>
				</div>

			</div>
		</section>

	<?php endwhile; endif; ?>
	
<?php get_footer(); ?>