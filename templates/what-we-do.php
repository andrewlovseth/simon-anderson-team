<?php

/* Template Name: What We Do */

get_template_part('template-parts/header/template-options');

get_header(); ?>

	<?php get_template_part('partials/hero'); ?>

	<section class="services">
		<div class="wrapper">
			
			<?php if(have_rows('services')): $count = 1; while(have_rows('services')): the_row(); ?>
			 
			    <div class="service" id="<?php echo sanitize_title_with_dashes(get_sub_field('headline')); ?>" <?php get_template_part('partials/animations/fade-left'); ?>>
			    	<div class="number">
			    		<div class="headline">
			    			<h3>0<?php echo $count; ?></h3>
			    		</div>			    		
			    	</div>

			    	<div class="icon">
			    		<img src="<?php $image = get_sub_field('icon'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
			    	</div>

			    	<div class="info">
			    		<div class="headline">
			    			<h3><?php the_sub_field('headline'); ?></h3>
			    		</div>

			    		<div class="copy p2">
			    			<?php the_sub_field('copy'); ?>
			    		</div>

			    		<div class="cta">
			    			<?php if(have_rows('cta')): while(have_rows('cta')) : the_row(); ?>
 
							    <?php if( get_row_layout() == 'page_link' ): ?>
									
									<?php $link = get_sub_field('link'); if( $link ): $link_url = $link['url']; $link_title = $link['title']; ?>
										<a class="btn charcoal" href="<?php echo esc_url($link_url); ?>"><?php echo esc_html($link_title); ?></a>
									<?php endif; ?>
									
							    <?php endif; ?>

							    <?php if( get_row_layout() == 'contact_agent' ): ?>
							    	<?php $listing_agent = get_sub_field('agent'); ?>
	
									<a href="#" class="btn charcoal agent-trigger" data-agent="<?php echo get_permalink($listing_agent->ID); ?>"><?php the_sub_field('cta_label'); ?></a>
							    <?php endif; ?>
							 
							<?php endwhile; endif; ?>

			    		</div>			    		
			    	</div>
			        
			    </div>

			<?php $count++; endwhile; endif; ?>

		</div>
	</section>

	<?php get_template_part('partials/big-ctas-header'); ?>

	<section class="big-ctas">
		<div class="cta charcoal process">
			<div class="content">
				<div class="content-wrapper">
						
					<div class="icon">
						<a href="<?php echo site_url('/how-we-do-it/'); ?>">
							<img src="<?php bloginfo('template_directory') ?>/images/process-icon.svg" alt="Process Icon" />
						</a>				
					</div>

					<div class="headline">
						<h3>See how our process leads you to certainty</h3>
					</div>

					<div class="btn-wrapper">
						<a href="<?php echo site_url('/how-we-do-it/'); ?>" class="btn clear-white">View Process</a>
					</div>

				</div>			
			</div>
		</div>

		<div class="cta yellow contact">
			<div class="content">
				<div class="content-wrapper">
					
					<div class="icon">
						<a href="<?php echo site_url('/contact/'); ?>">
							<img src="<?php bloginfo('template_directory') ?>/images/contact-icon.svg" alt="Contact Icon" />
						</a>				
					</div>

					<div class="headline">
						<h3>Have questions? Give us a call</h3>
					</div>

					<div class="btn-wrapper">
						<a href="<?php echo site_url('/contact/'); ?>" class="btn clear-white">Contact Us</a>
					</div>

				</div>			
			</div>
		</div>
	</section>
	
<?php get_footer(); ?>