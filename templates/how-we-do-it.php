<?php

/* Template Name: How We Do It */

get_template_part('template-parts/header/template-options');

get_header(); ?>

	<?php get_template_part('partials/hero'); ?>

	
	<section class="processes-nav">
		<div class="wrapper">
			
			<?php if(have_rows('process')): $count = 1; while(have_rows('process')): the_row(); ?>

				<div class="anchor">
				    <a href="#<?php echo sanitize_title_with_dashes(get_sub_field('headline')); ?>">
				    	<span class="number">0<?php echo $count; ?></span>
				    	<span class="label"><?php the_sub_field('headline'); ?></span>			        
				    </a>
				</div>
			 
			 <?php $count++; endwhile; endif; ?>

		</div>
	</section>

	<section id="process" class="processes">
		<div class="wrapper">
			
			<?php if(have_rows('process')): $count = 1; while(have_rows('process')): the_row(); ?>
			 
			    <div id="<?php echo sanitize_title_with_dashes(get_sub_field('headline')); ?>" class="process" <?php get_template_part('partials/animations/fade-left'); ?>>
			    	<div class="number">
			    		<div class="headline">
			    			<h3>0<?php echo $count; ?></h3>
			    		</div>			    		
			    	</div>

			    	<div class="icon">
			    		<img src="<?php $image = get_sub_field('icon'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
			    	</div>

			    	<div class="info">
			    		<div class="headline">
			    			<h2><?php the_sub_field('headline'); ?></h2>
			    			<h3><?php the_sub_field('sub_headline'); ?></h3>
			    		</div>

			    		<div class="copy p2">
			    			<?php the_sub_field('copy'); ?>
			    		</div>			    		
			    	</div>
			        
			    </div>

			<?php $count++; endwhile; endif; ?>

		</div>
	</section>

	<?php get_template_part('partials/big-ctas-header'); ?>

	<section class="big-ctas">
		<div class="cta charcoal process">
			<div class="content">
				<div class="content-wrapper">
						
					<div class="icon">
						<a href="<?php echo site_url('/team/'); ?>">
							<img src="<?php bloginfo('template_directory') ?>/images/team-icon.svg" alt="Team Icon" />
						</a>				
					</div>

					<div class="headline">
						<h3><?php the_field('team_headline'); ?></h3>
					</div>

					<div class="btn-wrapper">
						<a href="<?php echo site_url('/team/'); ?>" class="btn clear-white">View Team</a>
					</div>

				</div>			
			</div>
		</div>

		<div class="cta yellow contact">
			<div class="content">
				<div class="content-wrapper">
					
					<div class="icon">
						<a href="<?php echo site_url('/contact/'); ?>">
							<img src="<?php bloginfo('template_directory') ?>/images/case-studies-icon.svg" alt="Case Studies Icon" />
						</a>				
					</div>

					<div class="headline">
						<h3><?php the_field('case_studies_headline'); ?></h3>
					</div>

					<div class="btn-wrapper">
						<a href="<?php $case_study = get_field('case_study'); echo get_permalink($case_study->ID); ?>" class="btn clear-white">View Recent Case Study</a>
					</div>

				</div>			
			</div>
		</div>
	</section>


<?php get_footer(); ?>