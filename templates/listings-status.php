<?php

/* Template Name: Listing Status */

get_header(); ?>

	<div class="section-header listings-section-header">
		<div class="wrapper">
			<h4>Property Listings: <?php the_title(); ?></h4>
		</div>
	</div>

	<section class="filters listings-filters">
		<div class="wrapper">

			<div class="property-type">
				<div class="header">
					<h5>Property Type</h5>
				</div>

				<div class="toggles">
					<div class="toggle">
						<a href="#" data-filter="under-50-units">Under<br/>50 Units</a>
					</div>

					<div class="toggle">
						<a href="#" data-filter="50-plus-units">50+<br/>Units</a>
					</div>

					<div class="toggle">
						<a href="#" data-filter="micro-and-efficiency">Mirco &<br/>Efficency</a>
					</div>

					<div class="toggle">
						<a href="#" data-filter="development-land">Development<br/>Land</a>
					</div>
				</div>
			</div>

			<div class="views">
				<a href="<?php echo site_url('/listings/'); ?>" class="active view-trigger"><span>List View</span></a>
				<a href="<?php echo site_url('/map/'); ?>" class="view-trigger"><span>Map View</span></a>
			</div>

		</div>
	</section>

	<section class="main listings-main">
		<div class="wrapper">
	
			<div id="response" class="listings" data-status="<?php global $post; $post_slug = $post->post_name; echo $post_slug; ?>">

			</div>

		</div>
	</section>
	
<?php get_footer(); ?>