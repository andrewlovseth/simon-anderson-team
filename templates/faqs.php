<?php

/* Template Name: FAQs */

get_header(); ?>

	<section class="hero">
		<div class="wrapper">

			<div class="info">
				<div class="headline">
					<h4>Contact Us / <?php the_title(); ?></h4>
				</div>
			</div>		

		</div>
	</section>

	<?php if(have_rows('faqs')): while(have_rows('faqs')) : the_row(); ?>	 
	    <?php if( get_row_layout() == 'section' ): ?>
			
			<section class="faq" id="<?php echo sanitize_title_with_dashes(get_sub_field('section_headline')); ?>">
				<div class="wrapper">

					<div class="headline section-headline">
						<h2><?php the_sub_field('section_headline'); ?></h2>
					</div>

					<div class="questions">
						<?php if(have_rows('questions')): while(have_rows('questions')): the_row(); ?>
						 
						    <div class="question-wrapper">
						    	<div class="question headline">
						    		<h3><?php the_sub_field('question'); ?></h3>
									<div class="toggle"></div>
						    	</div>

						    	<div class="answer copy p3">
						    		<div class="copy-wrapper">
						    			<?php the_sub_field('answers'); ?>
						    		</div>
						    	</div>					        
						    </div>

						<?php endwhile; endif; ?>					
					</div>
					
				</div>    		
			</section>
			
	    <?php endif; ?>	 
	<?php endwhile; endif; ?>
	
<?php get_footer(); ?>