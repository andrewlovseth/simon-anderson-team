<?php

/* Template Name: Why Us */

get_template_part('template-parts/header/template-options');

get_header(); ?>

	<?php get_template_part('partials/hero'); ?>

	<section class="processes">
		<div class="wrapper">
			
			<?php if(have_rows('process')): $count = 1; while(have_rows('process')): the_row(); ?>
			 
			    <div class="process process-<?php echo $count; ?>">
			    	<div class="image" <?php get_template_part('partials/animations/fade-right'); ?>>
			    		<img src="<?php $image = get_sub_field('image'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
			    	</div>	    	

			    	<div class="info">
			    		<div class="info-wrapper" <?php get_template_part('partials/animations/fade-left'); ?>>
				    		<div class="process-header headline">
				    			<h2>0<?php echo $count; ?></h2>
				    			<h4><?php the_sub_field('process_name'); ?></h4>
				    		</div>	

				    		<div class="headline main-headline">
				    			<h3><?php the_sub_field('headline'); ?></h3>
				    		</div>

				    		<div class="copy p2">
				    			<?php the_sub_field('copy'); ?>
				    		</div>

				    		<div class="cta">
								<?php $link = get_sub_field('cta'); if( $link ): $link_url = $link['url']; $link_title = $link['title']; ?>
									<a class="btn charcoal" href="<?php echo esc_url($link_url); ?>"><?php echo esc_html($link_title); ?></a>
								<?php endif; ?>
				    		</div>			    			
			    		</div>			    		
			    	</div>			        
			    </div>

			<?php $count++; endwhile; endif; ?>

		</div>
	</section>


	<section class="case-studies-header">
		<div class="headline">
			<h2>Case Studies</h2>
		</div>
	</section>


	<section class="case-studies">

		<?php $posts = get_field('case_studies'); if( $posts ): ?>
			<?php foreach( $posts as $p ): ?>

				<div class="case-study">

					<div class="photo">
						<div class="content">

							<a href="<?php echo get_permalink($p->ID); ?>">
								<img src="<?php $image = get_field('hero_image', $p->ID); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
							</a>
							
						</div>
					</div>

					<div class="info">
						<div class="info-wrapper">

							<div class="headline">
								<h3><a href="<?php echo get_permalink( $p->ID ); ?>"><?php echo get_the_title( $p->ID ); ?></a></h3>
							</div>

							<div class="copy p2">
								<p><?php the_field('teaser_copy', $p->ID); ?></p>
							</div>

							<div class="cta">
								<a href="<?php echo get_permalink( $p->ID ); ?>" class="btn charcoal">View Case Study</a>
							</div>						
							
						</div>
					</div>

				</div>
			<?php endforeach; ?>
		<?php endif; ?>

	</section>


	<?php get_template_part('partials/testimonials'); ?>

<?php get_footer(); ?>