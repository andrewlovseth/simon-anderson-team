<?php

/* Template Name: Market Updates */

get_header(); ?>

	<section class="page-header">
		<div class="wrapper">
			
			<div class="headline">
				<h4>Market Updates</h4>
			</div>

		</div>
	</section>

	<?php get_template_part('partials/newsletter-sign-up'); ?>

	<section class="updates" <?php get_template_part('partials/animations/fade-up'); ?>>
		<section class="research content-type">
			<div class="section-header headline">
				<h3><span>Research</span></h3>
			</div>

			<div class="featured-photo">
				<div class="content">
					<img src="<?php $image = get_field('featured_research_photo'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />								
				</div>
			</div>

			<div class="posts">
				<?php $posts = get_field('featured_research'); if( $posts ): ?>
				    <?php foreach( $posts as $post): setup_postdata($post); ?>

						<?php
							$file = get_field('file');
							$file_type = wp_check_filetype( $file['url'] );
							$file_size = filesize( get_attached_file($file['id']));
							$entry_page = get_field('entry_page');
						?>

						<div class="post post-research">
							<div class="info-wrapper">
								<div class="info">
									<div class="headline">
										<h3><a href="<?php echo $file['url']; ?><?php if($entry_page): ?>#page=<?php echo $entry_page; ?><?php endif; ?>" rel="external"><?php the_title(); ?></a></h3>		
									</div>

									<div class="meta">
										<p><?php the_time('n/j/y'); ?></p>
									</div>	
								</div>

								<div class="cta">
									<a href="<?php echo $file['url']; ?><?php if($entry_page): ?>#page=<?php echo $entry_page; ?><?php endif; ?>" class="btn charcoal" rel="external">Download Report</a>
								</div>
							</div>
						</div>

				    <?php endforeach;  wp_reset_postdata(); ?>
				<?php endif; ?>		
			</div>	

			<div class="view-all cta">
				<div class="link-wrapper">
					<a href="<?php echo site_url('/research/'); ?>" class="btn yellow">View all research</a>
				</div>
			</div>	
		</section>


		<section class="blog content-type">
			<div class="section-header headline">
				<h3><span>Blog</span></h3>
			</div>

			<div class="featured-photo">
				<div class="content">
					<img src="<?php $image = get_field('featured_blog_photo'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
				</div>
			</div>

			<div class="posts">
				<?php $posts = get_field('featured_posts'); if( $posts ): ?>
				    <?php foreach( $posts as $post): setup_postdata($post); ?>
						
						<div class="post">

							<div class="info">
								<div class="info-wrapper">
									<div class="headline">
										<h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
									</div>

									<div class="meta">
										<p><?php the_time('n/j/y'); ?>: <a href="<?php $author = get_field('author'); echo get_permalink($author->ID); ?>"><?php echo get_the_title($author->ID); ?></a> | 	<?php $categories = get_the_category(); if ( ! empty( $categories ) ): ?><a href="<?php echo get_category_link($categories[0]->term_id); ?>"><?php echo $categories[0]->name; ?></a><?php endif; ?></p>
									</div>	

									<div class="cta">
										<a href="<?php the_permalink(); ?>" class="btn clear-white">Read More</a>
									</div>
								</div>
							</div>

						</div>

				    <?php endforeach;  wp_reset_postdata(); ?>
				<?php endif; ?>		
			</div>

			<div class="view-all cta">
				<div class="link-wrapper">
					<a href="<?php echo site_url('/blog/'); ?>" class="btn yellow">See all blog posts</a>
				</div>
			</div>

		</section>
	</section>

	
<?php get_footer(); ?>

