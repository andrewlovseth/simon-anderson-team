<?php

/* Template Name: Team */

get_header(); ?>


	<section class="hero">
		<div class="wrapper">

			<div class="info">
				<div class="headline">
					<h4>Why Us / <?php the_title(); ?></h4>
					<h1><?php the_field('hero_headline'); ?></h1>
				</div>

				<div class="copy p1">
					<p><?php the_field('hero_dek'); ?></p>	
				</div>

				<div class="about copy p3 extended">
					<?php the_field('about_team_copy'); ?>
				</div>
			</div>		

		</div>
	</section>


	<section class="team-members">
		<div class="wrapper">

			<div class="team-wrapper">
				<?php $agents = get_field('team_members'); if( $agents ): ?>
					<?php foreach( $agents as $a ): ?>
						<div class="team-member">
							<div class="photo">
								<a href="<?php echo get_permalink($a->ID); ?>">
									<img src="<?php $image = get_field('photo', $a->ID); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
								</a>
							</div>

							<div class="info-wrapper">
								<div class="info">
									<div class="row name headline">
										<h3><a href="<?php echo get_permalink($a->ID); ?>"><?php echo get_the_title($a->ID); ?></a></h3>
									</div>

									<?php if(get_field('title', $a->ID)): ?>
										<div class="row title copy p2">
											<p><?php the_field('title', $a->ID); ?></p>
										</div>
									<?php endif; ?>	

									<?php if(get_field('direct_phone', $a->ID)): ?>
										<div class="row direct copy p2">
											<p>Direct: <?php the_field('direct_phone', $a->ID); ?></p>
										</div>
									<?php endif; ?>		

									<?php if(get_field('email', $a->ID)): ?>
										<div class="row email copy p2">
											<p><a href="mailto:<?php the_field('email', $a->ID); ?>"><?php the_field('email', $a->ID); ?></a></p>
										</div>
									<?php endif; ?>						
								</div>

								<div class="linkedin">
									<a href="<?php the_field('linkedin', $a->ID); ?>" rel="external"><img src="<?php bloginfo('template_directory') ?>/images/linkedin.svg" alt="LinkedIn" /></a>
								</div>

								<div class="bio copy p3">
									<?php the_field('short_bio', $a->ID); ?>
								</div>

								<div class="cta">
									<a href="<?php echo get_permalink($a->ID); ?>" class="btn charcoal">View Full Bio</a>
								</div>								
							</div>

					    </div>
					<?php endforeach; ?>
				<?php endif; ?>
			</div>

		</div>
	</section>

	<?php get_template_part('partials/big-ctas-header'); ?>

	<section class="big-ctas">
		<div class="cta charcoal process">
			<div class="content">
				<div class="content-wrapper">
						
					<div class="icon">
						<a href="<?php echo site_url('/how-we-do-it/'); ?>">
							<img src="<?php bloginfo('template_directory') ?>/images/process-icon.svg" alt="Process Icon" />
						</a>				
					</div>

					<div class="headline">
						<h3>See how our process leads you to certainty</h3>
					</div>

					<div class="btn-wrapper">
						<a href="<?php echo site_url('/how-we-do-it/'); ?>" class="btn clear-white">View Process</a>
					</div>

				</div>			
			</div>
		</div>

		<div class="cta yellow contact">
			<div class="content">
				<div class="content-wrapper">
					
					<div class="icon">
						<a href="<?php echo site_url('/contact/'); ?>">
							<img src="<?php bloginfo('template_directory') ?>/images/contact-icon.svg" alt="Contact Icon" />
						</a>				
					</div>

					<div class="headline">
						<h3>Have questions? Give us a call</h3>
					</div>

					<div class="btn-wrapper">
						<a href="<?php echo site_url('/contact/'); ?>" class="btn clear-white">Contact Us</a>
					</div>

				</div>			
			</div>
		</div>
	</section>

<?php get_footer(); ?>