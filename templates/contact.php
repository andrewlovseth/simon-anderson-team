<?php

/* Template Name: Contact */

get_template_part('template-parts/header/template-options');

get_header(); ?>

	<section class="hero cover" style="background-image: url(<?php $image = get_field('hero_image'); echo $image['url']; ?>);">
		<div class="content">
			<div class="wrapper">

				<div class="info">
					<div class="headline">
						<h4><?php the_title(); ?></h4>
						<h1><?php the_field('hero_headline'); ?></h1>
					</div>
				</div>		

			</div>
		</div>
	</section>


	<section class="general-inquiries" <?php get_template_part('partials/animations/fade-up'); ?>>
		<div class="wrapper">

			<div class="headline">
				<h3><?php the_field('contact_form_headline'); ?></h3>
			</div>

			<?php 
				$shortcode = get_field('contact_form');
				echo do_shortcode($shortcode);
			?>
			
		</div>
	</section>

	<section class="location">
		<div class="info" <?php get_template_part('partials/animations/fade-right'); ?>>
			<div class="contact-info">
				<div class="contact-wrapper">
					<div class="headline address">
						<h4>Address</h4>
						<h3><?php the_field('address'); ?></h3>
					</div>

					<div class="headline phone">
						<h4>Phone</h4>
						<h3><?php the_field('phone'); ?></h3>
					</div>
				</div>
			</div>

			<div class="photo">
				<img src="<?php $image = get_field('location_photo'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
			</div>
		</div>

		<div class="map" <?php get_template_part('partials/animations/fade-left'); ?>>
			<?php get_template_part('partials/map/contact-map'); ?>
		</div>
	</section>

	<section class="questions">
		<div class="wrapper">
			
			<div class="headline section-headline" <?php get_template_part('partials/animations/fade-up'); ?>>
				<h2><?php the_field('questions_headline'); ?></h2>
			</div>

			<?php if(have_rows('questions')): while(have_rows('questions')) : the_row(); ?>
			 
			    <?php if( get_row_layout() == 'section' ): ?>
					
					<section class="topic-section" <?php get_template_part('partials/animations/fade-up'); ?>>
						<?php if(have_rows('topics')): while(have_rows('topics')): the_row(); ?>
							<div class="topic">
							    <div class="icon">
							        <img src="<?php $image = get_sub_field('icon'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
							    </div>

							    <div class="headline">
							    	<h3><?php the_sub_field('title'); ?></h3>
							    </div>
							</div>
						<?php endwhile; endif; ?>

						<div class="cta">
					    	<?php $listing_agent = get_sub_field('agent'); ?>

							<a href="#" class="btn yellow agent-trigger" data-agent="<?php echo get_permalink($listing_agent->ID); ?>"><?php the_sub_field('cta_label'); ?></a>

						</div>
					</section>
					
			    <?php endif; ?>
			 
			<?php endwhile; endif; ?>
		</div>
	</section>


	<section class="big-ctas">
		<div class="cta charcoal faqs">
			<div class="content">
				<div class="content-wrapper">
						
					<div class="icon">
						<a href="<?php echo site_url('/how-we-do-it/'); ?>">
							<img src="<?php bloginfo('template_directory') ?>/images/faqs-icon.svg" alt="FAQs Icon" />
						</a>				
					</div>

					<div class="headline">
						<h3>FAQs</h3>
					</div>

					<div class="btn-wrapper">
						<a href="<?php echo site_url('/contact/faqs/'); ?>" class="btn clear-white">See FAQs</a>
					</div>

				</div>			
			</div>
		</div>

		<div class="cta yellow valuation">
			<div class="content">
				<div class="content-wrapper">
					
					<div class="icon">
						<a href="<?php echo site_url('/#valuation'); ?>">
							<img src="<?php bloginfo('template_directory') ?>/images/valuation-icon-white.svg" alt="Valuation Icon" />
						</a>				
					</div>

					<div class="headline">
						<h3>Get Online Valuation</h3>
					</div>

					<div class="btn-wrapper">
						<a href="<?php echo site_url('/#valuation'); ?>" class="btn clear-white">Get Valuation</a>
					</div>

				</div>			
			</div>
		</div>
	</section>


<?php get_footer(); ?>