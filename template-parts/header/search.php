<?php 
	global $theme;
	global $hero;
?>

<div class="search-field desktop">
	<a href="#" class="search-trigger">
		<?php if($theme == 'light'): ?>
			<img id="search-icon-white" src="<?php bloginfo('template_directory') ?>/images/search-icon.svg" alt="Search" data-dark="<?php bloginfo('template_directory') ?>/images/search-icon.svg" data-white="<?php bloginfo('template_directory') ?>/images/search-icon.svg" />
		<?php else: ?>
			<img src="<?php bloginfo('template_directory') ?>/images/search-icon.svg" alt="Search" />
		<?php endif; ?>
	</a>
</div>