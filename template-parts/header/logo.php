<div class="logo">
	<a href="<?php echo site_url('/'); ?>">
		<span class="image">
			<img src="<?php $image = get_field('logo', 'options'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>"	 />
		</span>

		<?php if(get_field('site_name', 'options')): ?>
			<span class="site-name">
				<?php the_field('site_name', 'options'); ?>
			</span>
		<?php endif; ?>
	</a>
</div>