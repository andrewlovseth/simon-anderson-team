<nav class="main-nav">

	<?php if(have_rows('header_navigation', 'options')): while(have_rows('header_navigation', 'options')): the_row(); ?>
	 
	    <div class="link">
			<?php 

			$link = get_sub_field('link');

			if( $link ): 
				$link_url = $link['url'];
				$link_title = $link['title'];
				?>
				<a href="<?php echo esc_url($link_url); ?>"><?php echo esc_html($link_title); ?></a>
			<?php endif; ?>
		</div>

	<?php endwhile; endif; ?>


	<div class="search-field mobile">

		<form role="search" method="get" class="search-form" action="<?php echo home_url( '/' ); ?>">
	    	<input type="submit" class="search-submit" value="" />
	        <input type="search" class="search-query" autocomplete="off" value="" name="s" />
	    </form>
		
	</div>


</nav>

<a href="#" id="toggle">
	<div class="patty"></div>
</a>		
