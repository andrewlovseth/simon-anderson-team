<?php

get_template_part('template-parts/header/template-options');

get_header(); ?>


	<section class="hero cover" style="background-image: url(<?php $image = get_field('hero_image'); echo $image['url']; ?>);">
		<div class="content">
			<div class="wrapper">

				<div class="info">
					<div class="status">
						<?php
							$status = get_field('status');
							$label = preg_replace("/[\-_]/", " ", $status);
							$label = substr($label, 1);
						?>

						<img class="icon" src="<?php bloginfo('template_directory') ?>/images/status/<?php echo $status; ?>_dark.svg" alt="">
						<span class="label"><?php echo $label; ?></span>
					</div>

					<div class="headline">
						<h4>Listings/<?php the_title(); ?></h4>
						<h1><?php the_title(); ?></h1>
					</div>
				</div>

			</div>
		</div>
	</section>


	<section class="ctas">
		<?php $status = get_field('status'); if($status == 'a_on-the-market'): ?>
			<?php if(get_field('marketing_materials')): ?>
				<div class="cta marketing-materials">
					<a href="<?php the_field('marketing_materials'); ?>" rel="external">Get Marketing Materials</a>
				</div>
			<?php endif; ?>
		<?php endif; ?>

		<?php $listing_agent = get_field('primary_listing_agent'); if( $listing_agent ): ?>
			<div class="cta listing-agent">
				<a href="#" class="agent-trigger" data-agent="<?php echo get_permalink($listing_agent->ID); ?>">Contact Listing Agent</a>
			</div>
		<?php endif; ?>
	</section>


	<?php if(get_field('status_copy') || get_field('logo')): ?>

		<section class="status">
			<div class="wrapper">

				<?php if(get_field('logo')): ?>
					<div class="logo" <?php get_template_part('partials/animations/fade-right'); ?>>
						<img src="<?php $image = get_field('logo'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
					</div>
				<?php endif; ?>

				<?php if(get_field('status_copy')): ?>
					<div class="info" <?php get_template_part('partials/animations/fade-left'); ?>>
						<?php if(get_field('status_headline')): ?>
							<div class="headline">
								<h3><?php the_field('status_headline'); ?></h3>
							</div>
						<?php endif; ?>

						<div class="copy p2">
							<?php the_field('status_copy'); ?>
						</div>
				
						<?php 
							$link = get_field('status_link');
							if( $link ): 
								$link_url = $link['url'];
								$link_title = $link['title'];
								$link_target = $link['target'] ? $link['target'] : '_self'; ?>
							<div class="cta">
								<a class="btn charcoal" href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr( $link_target ); ?>"><?php echo esc_html($link_title); ?></a>
							</div>
						<?php endif; ?>
					</div>
				<?php endif; ?>


			</div>
		</section>

	<?php endif; ?>
	
	<?php if(get_field('summary_copy')): ?>
		<section class="summary">
			<div class="wrapper">

				<?php if(get_field('summary_headline')): ?>
					<div class="headline" <?php get_template_part('partials/animations/fade-left'); ?>>
						<h2><?php the_field('summary_headline'); ?></h2>
					</div>
				<?php endif; ?>

				<div class="copy p2" <?php get_template_part('partials/animations/fade-left'); ?>>
					<?php the_field('summary_copy'); ?>
				</div>

			</div>
		</section>
	<?php endif; ?>

	<?php if(have_rows('site_details') || have_rows('development_details')): ?>

		<section class="details">
			<div class="wrapper">

				<?php $site_details_headline = get_field('site_details_headline'); if(have_rows('site_details')): ?>
					<div class="site-details details-table" <?php get_template_part('partials/animations/fade-up'); ?>>
						<div class="headline">
							<?php if($site_details_headline): ?>
								<h3><?php echo $site_details_headline; ?></h3>
							<?php else: ?>
								<h3>Site Details</h3>
							<?php endif; ?>
						</div>
					
						<div class="table">
							<?php while(have_rows('site_details')): the_row(); ?>
								 
							    <div class="row">
							    	<div class="key">
							    		<span><?php the_sub_field('key'); ?></span>
							    	</div>

							    	<div class="value">
							    		<span><?php the_sub_field('value'); ?></span>
							    	</div>
							    </div>

							<?php endwhile; ?>
						</div>					
					</div>
				<?php endif; ?>

				<?php $development_details_headline = get_field('development_details_headline');if(have_rows('development_details')): ?>
					<div class="development-details details-table" <?php get_template_part('partials/animations/fade-up'); ?>>
						<div class="headline">
							<?php if($development_details_headline): ?>
								<h3><?php echo $development_details_headline; ?></h3>
							<?php else: ?>
								<h3>Development Details</h3>
							<?php endif; ?>
						</div>

						<div class="table">
							<?php while(have_rows('development_details')): the_row(); ?>
								 
							    <div class="row">
							    	<div class="key">
							    		<span><?php the_sub_field('key'); ?></span>
							    	</div>

							    	<div class="value">
							    		<span><?php the_sub_field('value'); ?></span>
							    	</div>
							    </div>

							<?php endwhile; ?>
						</div>
					</div>
				<?php endif; ?>

			</div>
		</section>
	<?php endif; ?>

	<?php $images = get_field('summary_gallery'); if( $images || get_field('summary_video')): ?>
		<section class="details-gallery<?php if(!$images): ?> no-images<?php endif; ?>" <?php get_template_part('partials/animations/fade-up'); ?>>
			<div class="wrapper">
				<div class="gallery">

					<?php if(get_field('summary_video')): ?>
						<div class="photo video">
							<div class="content">
								<iframe src="//www.youtube-nocookie.com/embed/<?php the_field('summary_video'); ?>?rel=0&showinfo=0&modestbranding=1&vq=hd1080&autoplay=0" frameborder="0" width="1920" height="1080" allowfullscreen></iframe>								
							</div>

						</div>
					<?php endif; ?>		
					
					<?php if($images): ?>
						<?php foreach( $images as $image ): ?>

							<div class="photo">
								<img src="<?php echo $image['sizes']['large']; ?>" alt="<?php echo $image['alt']; ?>" />
							</div>
							
						<?php endforeach; ?>
					<?php endif; ?>

				</div>
			</div>
		</section>
	<?php endif; ?>


	<?php if(get_field('opportunity_copy') || have_rows('opportunity_features')): ?>
		<section class="opportunity features-section">
			<div class="wrapper">
				
				<div class="info">
					<?php if(get_field('opportunity_headline')): ?>
						<div class="headline" <?php get_template_part('partials/animations/fade-right'); ?>>
							<h2><?php the_field('opportunity_headline'); ?></h2>
						</div>
					<?php endif; ?>

					<div class="copy p2" <?php get_template_part('partials/animations/fade-right'); ?>>
						<?php the_field('opportunity_copy'); ?>
					</div>
				</div>

				<?php if(have_rows('opportunity_features')): ?>
					<div class="features with-icon">
						<?php while(have_rows('opportunity_features')): the_row(); ?>
		 
						    <div class="feature"  <?php get_template_part('partials/animations/fade-left'); ?>>
						    	<?php if(get_sub_field('icon')): ?>
							    	<div class="icon">
							    		<img src="<?php $image = get_sub_field('icon'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
							    	</div>
							    <?php endif; ?>

						    	<div class="feature-info">
						    		<div class="headline">
						    			<h3><?php the_sub_field('headline'); ?></h3>
						    		</div>

						    		<div class="copy p2">
						    			<p><?php the_sub_field('deck'); ?></p>
						    		</div>				    		
						    	</div>				        
						    </div>

						<?php endwhile; ?>
					</div>
				<?php endif; ?>

			</div>
		</section>
	<?php endif; ?>

	
	<?php if(get_field('map')): ?>
		<section class="location">
			<div class="headline" <?php get_template_part('partials/animations/fade-up'); ?>>
				<div class="wrapper">
					<h2>Location</h2>
					<a href="<?php the_field('google_maps_link'); ?>" rel="external" class="btn yellow" id="google-maps-link">Open Google Maps</a>
				</div>
			</div>

			<?php get_template_part('partials/map/listing-map'); ?>
		</section>
	<?php endif; ?>

	<?php if(get_field('area_copy')): ?>
		
		<section class="about-the-area features-section">
			<div class="wrapper">

				<div class="info" <?php get_template_part('partials/animations/fade-right'); ?>>
					<?php if(get_field("area_headline")): ?>
						<div class="headline">
							<h2><?php the_field('area_headline'); ?></h2>
						</div>
					<?php endif; ?>

					<div class="copy p2">
						<?php the_field('area_copy'); ?>
					</div>
				</div>

				<?php if(have_rows('area_features')): ?>
					<div class="features" <?php get_template_part('partials/animations/fade-left'); ?>>
						<?php while(have_rows('area_features')): the_row(); ?>
		 
						    <div class="feature">
						    	<div class="feature-info">
						    		<div class="headline">
						    			<h3><?php the_sub_field('headline'); ?></h3>
						    		</div>

						    		<div class="copy p2">
						    			<p><?php the_sub_field('deck'); ?></p>
						    		</div>				    		
						    	</div>				        
						    </div>

						<?php endwhile; ?>
					</div>			
				<?php endif; ?>

			</div>
		</section>

	<?php endif; ?>


	<?php $images = get_field('gallery'); if( $images ): ?>
		<section class="gallery">
			<div class="main-photo" <?php get_template_part('partials/animations/fade-up'); ?>>
				<div class="content">				
					<div class="headline">
						<h2>Gallery</h2>
					</div>
					
					<img src="<?php $image = get_field('main_photo'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
				</div>
			</div>

			<div class="photos">
				<?php $count = 1; foreach( $images as $image ): ?>

					<div class="photo photo-<?php echo $count; ?>" <?php get_template_part('partials/animations/fade-up'); ?>>
						<div class="content">
							<img src="<?php echo $image['sizes']['large']; ?>" alt="<?php echo $image['alt']; ?>" />
						</div>						
					</div>
					
				<?php $count++; if($count == 11) { $count = 1; } endforeach; ?>
			</div>
		</section>
	<?php endif; ?>
	

	<?php $status = get_field('status'); if($status == 'a_on-the-market'): ?>
		<section class="big-ctas">
			<div class="cta yellow marketing-materials">
				<div class="content">
					<div class="content-wrapper">
							
						<div class="icon">
							<a href="<?php the_field('marketing_materials'); ?>" rel="external">
								<img src="<?php bloginfo('template_directory') ?>/images/marketing-materials-icon.svg" alt="Marketing Materials Icon" />
							</a>				
						</div>

						<div class="headline">
							<h3>Access marketing materials</h3>
						</div>

						<div class="btn-wrapper">
							<a href="<?php the_field('marketing_materials'); ?>" rel="external" class="btn clear-white">Get Materials</a>
						</div>

					</div>			
				</div>
			</div>

			<div class="cta charcoal contact">
				<div class="content">
					<div class="content-wrapper">
						
						<div class="icon">
							<a href="<?php echo site_url('/contact/'); ?>">
								<img src="<?php bloginfo('template_directory') ?>/images/contact-icon.svg" alt="Contact Icon" />
							</a>				
						</div>

						<div class="headline">
							<h3>Have questions? Give us a call</h3>
						</div>

						<?php $listing_agent = get_field('primary_listing_agent'); if( $listing_agent ): ?>
							<div class="btn-wrapper">
								<a href="#" class="btn clear-white agent-trigger" data-agent="<?php echo get_permalink($listing_agent->ID); ?>">Contact Us</a>
							</div>
						<?php else: ?>
							<div class="btn-wrapper">
								<a href="<?php echo site_url('/contact/'); ?>" class="btn clear-white">Contact Us</a>
							</div>
						<?php endif; ?>

					</div>			
				</div>
			</div>
		</section>
	<?php endif; ?>


	<section class="contact">
		<div class="wrapper">

			<div class="headline section-header">
				<h2><?php the_field('contact_headline'); ?></h2>
				<h3><?php the_field('contact_sub_headline'); ?></h3>
			</div>

			<div class="copy p2 deck">
				<?php the_field('contact_copy'); ?>
			</div>

			<div class="agents">
				<?php $agents = get_field('listing_agents'); if( $agents ): ?>
					<?php foreach( $agents as $a ): ?>
						<div class="agent">
							<div class="photo">
								<a href="<?php echo get_permalink($a->ID); ?>">
									<img src="<?php $image = get_field('photo', $a->ID); echo $image['sizes']['medium']; ?>" alt="<?php echo $image['alt']; ?>" />
								</a>
							</div>

							<div class="info">
								<div class="row name headline">
									<h3><a href="<?php echo get_permalink($a->ID); ?>"><?php echo get_the_title($a->ID); ?></a></h3>
								</div>

								<?php if(get_field('title')): ?>
									<div class="row title copy p2">
										<p><?php the_field('title', $a->ID); ?></p>
									</div>
								<?php endif; ?>

								<?php if(get_field('direct_phone', $a->ID)): ?>
									<div class="row direct copy p2">
										<p>Direct: <?php the_field('direct_phone', $a->ID); ?></p>
									</div>
								<?php endif; ?>

								<?php if(get_field('mobile_phone', $a->ID)): ?>
									<div class="row mobile copy p2">
										<p>Mobile: <?php the_field('mobile_phone', $a->ID); ?></p>
									</div>
								<?php endif; ?>

								<?php if(get_field('email', $a->ID)): ?>
									<div class="row email copy p2">
										<p><a href="mailto:<?php the_field('email', $a->ID); ?>"><?php the_field('email', $a->ID); ?></a></p>
									</div>
								<?php endif; ?>					
							</div>					    	
					    </div>
					<?php endforeach; ?>
				<?php endif; ?>
			</div>		

		</div>
	</section>
	
<?php get_footer(); ?>