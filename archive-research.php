<?php get_header(); ?>

	<section class="page-header" <?php get_template_part('partials/animations/fade-up'); ?>>
		<div class="wrapper">
			
			<div class="headline">
				<h4><a href="<?php echo site_url('/market-updates/'); ?>">Market Updates</a> / Research</h4>
			</div>

		</div>
	</section>

	<?php get_template_part('partials/newsletter-sign-up'); ?>

	<section class="featured-posts" <?php get_template_part('partials/animations/fade-up'); ?>>
		<?php

			$fps = get_field('featured_research', 'options'); if( $fps ): ?>
				   
			<?php foreach( $fps as $fp): ?>

				<div class="post">
					<div class="photo">
						<div class="content">
							<img src="<?php $image = get_field('hero_image', $fp->ID); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
						</div>
					</div>

					<div class="info">
						<div class="info-wrapper">

							<?php
								$file = get_field('file', $fp->ID);
								$file_type = wp_check_filetype( $file['url'] );
								$file_size = filesize( get_attached_file($file['id']));
								$entry_page = get_field('entry_page', $fp->ID);
							?>

							<div class="headline">
								<h3><a href="<?php echo $file['url']; ?><?php if($entry_page): ?>#page=<?php echo $entry_page; ?><?php endif; ?>" rel="external"><?php echo get_the_title($fp->ID); ?></a></h3>		
							</div>

							<div class="meta">
								<p><?php echo get_the_time('n/j/y', $fp->ID); ?></p>
							</div>	

							<div class="cta">
								<a href="<?php echo $file['url']; ?><?php if($entry_page): ?>#page=<?php echo $entry_page; ?><?php endif; ?>" class="btn clear-charcoal" rel="external">Download Report</a>
							</div>	
							
						</div>
					</div>
				</div>

		    <?php endforeach; ?>
		<?php endif; ?>		

	</section>

	<section class="topics">
		<div class="wrapper">

			<div class="topics-wrapper">
		
				<?php $terms = get_field('research_topics_order', 'options'); if( $terms ): ?>
					<?php $col = 1; $i =1; $row = 1; foreach( $terms as $term ): ?>

						<?php
							$slug = $term->slug;
							$args = array(
								'post_type' => 'research',
								'posts_per_page' => 25,
								'tax_query' => array(
									array(
										'taxonomy' => 'research_topics',
										'field'    => 'slug',
										'terms'    => $slug,
									),
								),
							);
							$query = new WP_Query( $args );
							if ( $query->have_posts() ) : ?>

							<div class="topic grid-col-<?php echo $col; ?> grid-row-<?php echo $row; ?>">
								<div class="headline section-header">
									<h4><?php echo $term->name; ?></h4>
								</div>

								<div class="docs">

									<?php while ( $query->have_posts() ) : $query->the_post(); ?>

										<?php
											$file = get_field('file');
											$file_type = wp_check_filetype( $file['url'] );
											$file_size = filesize( get_attached_file($file['id']));
											$entry_page = get_field('entry_page');
										?>


										<div class="doc">
											<div class="headline">
												<h3><a href="<?php echo $file['url']; ?><?php if($entry_page): ?>#page=<?php echo $entry_page; ?><?php endif; ?>" rel="external"><?php the_title(); ?></a></h3>		
											</div>

											<div class="meta">
												<p><?php the_time('n/j/y'); ?></p>
											</div>	
										</div>
									<?php endwhile; ?>

								</div>
							</div>
							
						<?php endif; wp_reset_postdata(); ?>

						<?php
							 if($col == 2) {
							 	$col = 1;
							 } else {
							 	$col++;
							 }

							 if($i % 2 == 0){
							 	$row++;
							 }
						?>

					<?php $i++; endforeach; ?>
				<?php endif; ?>

			</div>

		</div>
	</section>
	
<?php get_footer(); ?>