<div id="agent-modal">
	<div class="modal-wrapper">

		<a href="#" class="close"><img src="data:image/svg+xml,%3Csvg%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20width%3D%2224px%22%20height%3D%2224px%22%20viewBox%3D%220%200%2024%2024%22%20fill%3D%22%23FFFFFF%22%3E%0A%20%20%20%20%3Cpath%20d%3D%22M19%206.41L17.59%205%2012%2010.59%206.41%205%205%206.41%2010.59%2012%205%2017.59%206.41%2019%2012%2013.41%2017.59%2019%2019%2017.59%2013.41%2012z%22%2F%3E%0A%20%20%20%20%3Cpath%20d%3D%22M0%200h24v24H0z%22%20fill%3D%22none%22%2F%3E%0A%3C%2Fsvg%3E%0A" alt="X"></a>

		<div class="agent">
			<div class="photo">
				<a href="<?php the_permalink(); ?>">
					<img src="<?php $image = get_field('photo'); echo $image['sizes']['thumbnail']; ?>" alt="<?php echo $image['alt']; ?>" />
				</a>
			</div>

			<div class="info">
				<div class="row name headline">
					<h3><a href="<?php the_permalink(); ?>"><?php echo get_the_title(); ?></a></h3>
				</div>

				<?php if(get_field('title')): ?>
					<div class="row title copy p2">
						<p><?php the_field('title'); ?></p>
					</div>
				<?php endif; ?>

				<?php if(get_field('direct_phone')): ?>
					<div class="row direct copy p2">
						<p>Direct: <?php the_field('direct_phone'); ?></p>
					</div>
				<?php endif; ?>

				<?php if(get_field('email')): ?>
					<div class="row email copy p2">
						<p><a href="mailto:<?php the_field('email'); ?>"><?php the_field('email'); ?></a></p>
					</div>	
				<?php endif; ?>						
			</div>
		 </div>

	</div>
</div>