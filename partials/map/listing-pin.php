<?php
	$location = get_field('map');

	$status = get_field('status');
	$label = preg_replace("/[\-_]/", " ", $status);
	$label = substr($label, 1);

?>

<div class="marker" data-lat="<?php echo $location['lat']; ?>" data-lng="<?php echo $location['lng']; ?>">
	<div class="photo">
		<a href="<?php the_permalink(); ?>">
			<span class="mask">
				<img src="<?php $image = get_field('thumbnail'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
			</span>
			

			<div class="status">
				<img class="icon" src="<?php bloginfo('template_directory') ?>/images/status/<?php echo $status; ?>.svg" alt="">
				<span class="label"><?php echo $label; ?></span>
			</div>
		</a>
	</div>

	<div class="info">
		<h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
		<p>
			<?php the_field('address'); ?><br/>
			Units: <?php the_field('units'); ?><br/>
			Price: <?php the_field('price'); ?>
		</p>		

		<div class="cta">
			<a href="<?php the_permalink(); ?>" class="btn clear-white">See More</a>
		</div>
	</div>
</div>