<?php $cat_page = get_queried_object(); ?>

<section class="cat-nav">
	<div class="wrapper">

		<div class="links">

			<div class="label">
				<span>Topics:</span>
			</div>

			<div class="link all">
				<a href="<?php echo site_url('/blog/'); ?>">All</a>
			</div>
			
			<?php $terms = get_field('category_navigation', 'options'); if( $terms ): ?>
				<?php foreach( $terms as $term ): ?>

					<?php
						if($cat_page->slug == $term->slug) {
							$active_class = " active";
						} else {
							$active_class = "";
						}
					?>

					<div class="link <?php echo $term->slug; echo $active_class; ?>">
						<a href="<?php echo get_term_link( $term->term_id ); ?>"><?php echo $term->name; ?></a>
					</div>					

				<?php endforeach; ?>
			<?php endif; ?>
		</div>			
		
	</div>
</section>
