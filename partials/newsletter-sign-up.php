<section class="newsletter-sign-up" <?php get_template_part('partials/animations/fade-up'); ?>>
	<div class="wrapper">
		<div class="headline">
			<h3>Sign up for our newsletter</h3>
		</div>

		<div class="form">
			<a href="#subscribe" class="btn white smooth">Sign Up</a>
		</div>
	</div>
</section>