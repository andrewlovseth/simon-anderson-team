<div class="post">
	<div class="photo">
		<div class="content">
			<a href="<?php the_permalink(); ?>">
				<img src="<?php $image = get_field('hero_image'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
			</a>
		</div>
	</div>

	<div class="info">
		<div class="headline">
			<h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
		</div>

		<div class="meta">
			<p><?php the_time('n/j/y'); ?>: <a href="<?php $author = get_field('author'); echo get_permalink($author->ID); ?>"><?php echo get_the_title($author->ID); ?></a></p>
		</div>	
	</div>

	<div class="cta">
		<a href="<?php the_permalink(); ?>" class="btn charcoal">View Article</a>
	</div>
</div>