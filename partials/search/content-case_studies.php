<div class="result case-study">
	<div class="photo">
		<a href="<?php the_permalink(); ?>">
			<img src="<?php $image = get_field('hero_image'); echo $image['sizes']['thumbnail']; ?>" alt="<?php echo $image['alt']; ?>" />
		</a>
	</div>

	<div class="info">
		<div class="headline">
			<h4>Case Study</h4>
			<h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
		</div>

		<div class="copy p3">
			<p><?php the_field('teaser_copy'); ?></p>
		</div>

		<div class="cta">
			<a href="<?php the_permalink(); ?>" class="btn clear-charcoal">Read Case Study</a>
		</div>
	</div>
</div>