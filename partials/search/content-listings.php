<div class="result listing-result">
	<div class="photo">
		<a href="<?php the_permalink(); ?>">
			<img src="<?php $image = get_field('thumbnail'); echo $image['sizes']['thumbnail']; ?>" alt="<?php echo $image['alt']; ?>" />
		</a>
	</div>

	<div class="info">
		<div class="headline">
			<h4>Listing</h4>
			<h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
		</div>

		<div class="copy p3">
			<p>
				<?php the_field('address'); ?><br/>
				<strong>Units:</strong> <?php the_field('units'); ?><br/>
				<strong>Price:</strong> <?php the_field('price'); ?>
			</p>
		</div>

		<div class="cta">
			<a href="<?php the_permalink(); ?>" class="btn clear-charcoal">View Listing</a>
		</div>
	</div>
</div>