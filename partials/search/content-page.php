<div class="result page">
	<?php if(get_field('hero_image')): ?>
		<div class="photo">
			<a href="<?php the_permalink(); ?>">
				<img src="<?php $image = get_field('hero_image'); echo $image['sizes']['thumbnail']; ?>" alt="<?php echo $image['alt']; ?>" />
			</a>
		</div>
	<?php endif; ?>

	<div class="info<?php if(!get_field('hero_image')): ?> no-photo<?php endif; ?>">
		<div class="headline">
			<h4>Page</h4>
			<h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
		</div>

		<div class="cta">
			<a href="<?php the_permalink(); ?>" class="btn clear-charcoal">View Page</a>
		</div>
	</div>
</div>