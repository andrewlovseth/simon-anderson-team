<div class="result research">
	<div class="photo">
		<a href="<?php the_permalink(); ?>">
			<img src="<?php $image = get_field('hero_image'); echo $image['sizes']['thumbnail']; ?>" alt="<?php echo $image['alt']; ?>" />
		</a>
	</div>

	<?php
		$file = get_field('file', $fp->ID);
		$file_type = wp_check_filetype( $file['url'] );
		$file_size = filesize( get_attached_file($file['id']));
	?>

	<div class="info">
		<div class="headline">
			<h4>Research</h4>
			<h3><a href="<?php echo $file['url']; ?>" rel="external"><?php the_title(); ?></a></h3>
		</div>

		<div class="copy p3">
			<p><?php echo size_format( $file_size, $decimals = 2 ); ?> <?php echo $file_type['ext']; ?></p>
		</div>

		<div class="cta">
			<a href="<?php echo $file['url']; ?>" class="btn clear-charcoal" rel="external">Download Report</a>
		</div>
	</div>
</div>