<?php
	$file = get_field('file');
	$file_type = wp_check_filetype( $file['url'] );
	$file_size = filesize( get_attached_file($file['id']));
?>

<div class="post post-research">
	<div class="photo">
		<div class="content">
			<a href="<?php echo $file['url']; ?>" rel="external">
				<img src="<?php $image = get_field('hero_image'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
			</a>
		</div>
	</div>

	<div class="info">
		<div class="headline">
			<h3><a href="<?php echo $file['url']; ?>" rel="external"><?php the_title(); ?></a></h3>		
		</div>

		<div class="meta">
			<p><?php echo size_format( $file_size, $decimals = 2 ); ?> <?php echo $file_type['ext']; ?></p>
		</div>	
	</div>


	<div class="cta">
		<a href="<?php echo $file['url']; ?>" class="btn clear-charcoal" rel="external">Download <?php echo $file_type['ext']; ?></a>
	</div>
</div>