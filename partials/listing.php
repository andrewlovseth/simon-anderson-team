<?php
	$status = get_field('status');
	$label = preg_replace("/[\-_]/", " ", $status);
	$label = substr($label, 1);
	$property_type = get_field('property_type');
?>


<div class="listing <?php echo $status; ?>">
	<div class="photo">
		<a href="<?php the_permalink(); ?>">
			<img class="thumbnail" src="<?php $image = get_field('thumbnail'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />

			<div class="status">
				<img class="icon" src="<?php bloginfo('template_directory') ?>/images/status/<?php echo $status; ?>.svg" alt="">
				<span class="label"><?php echo $label; ?></span>
			</div>
		</a>
	</div>

	<div class="info">
		<h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
		<p>
			<?php the_field('address'); ?><br/>
			Units: <?php the_field('units'); ?><br/>
			Price: <?php the_field('price'); ?>
		</p>
		
	</div>

	<div class="agent">
		<?php if($status == 'c_under-contract'): ?>

			<div class="empty"></div>

		<?php elseif($status == 'd_sold'): ?>

			<?php if(get_field('case_study')): ?>
				<a href="#" class="contact-btn btn">View Case Study</a>
			<?php else: ?>
				<div class="empty"></div>
			<?php endif; ?>

		<?php else: ?>

			<?php $listing_agent = get_field('primary_listing_agent'); if( $listing_agent ): ?>
				<a href="#" class="contact-btn btn agent-trigger" data-agent="<?php echo get_permalink($listing_agent->ID); ?>">Contact Listing Agent</a>
			<?php endif; ?>

		<?php endif; ?>		
	</div>


</div>