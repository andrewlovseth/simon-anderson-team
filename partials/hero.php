<section class="hero cover" style="background-image: url(<?php $image = get_field('hero_image'); echo $image['url']; ?>);">
	<div class="content">
		<div class="wrapper">


			<div class="info">
				<div class="headline">
					<h4><?php the_title(); ?></h4>
					<h1><?php the_field('hero_headline'); ?></h1>
				</div>

				<div class="copy p1">
					<p><?php the_field('hero_dek'); ?></p>	
				</div>
			</div>		

		</div>
	</div>
</section>