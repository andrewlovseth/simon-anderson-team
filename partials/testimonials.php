<section class="testimonials">
	<div class="wrapper">

		<div class="testimonials-wrapper">
			<?php if(have_rows('testimonials')): while(have_rows('testimonials')): the_row(); ?>

			    <div class="testimonial">
			    	<div class="quote">
			    		<p><?php the_sub_field('quote'); ?></p>
			    	</div>

			    	<div class="source">
			    		<div class="image">
			    			<img src="<?php $image = get_sub_field('image'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
			    		</div>
			    		<p><?php the_sub_field('source'); ?></p>
			    	</div>
			        
			    </div>

			<?php endwhile; endif; ?>
		</div>			

	</div>
</section>