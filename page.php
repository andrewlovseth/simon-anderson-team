<?php get_header(); ?>

	<section class="generic page-header">
		<div class="wrapper">

			<div class="headline">
				<h2><?php the_title(); ?></h2>
			</div>

		</div>
	</section>

	<section class="generic copy p2 extended">
		<div class="wrapper">

			<?php if ( have_posts() ): while ( have_posts() ): the_post(); ?>
				<?php the_content(); ?>
			<?php endwhile; endif; ?>

		</div>
	</section>

<?php get_footer(); ?>

