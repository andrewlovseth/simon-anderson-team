<?php get_header(); ?>


	<section class="page-header">
		<div class="wrapper">
			
			<div class="headline">
				<h4><a href="<?php echo site_url('/market-updates/'); ?>">Market Updates</a> / <a href="<?php echo site_url('/blog/'); ?>">Blog</a> / <?php single_cat_title(''); ?></h4>
			</div>

		</div>
	</section>

	<?php
		get_template_part('partials/newsletter-sign-up');
		get_template_part('partials/cat-nav');
	?>

	<section class="cat-page-title">
		<div class="wrapper">

			<div class="headline">
				<h2><?php single_cat_title(''); ?></h2>
			</div>
			
		</div>
	</section>

	<section class="blog-posts">
		<div class="wrapper">
		
			<?php if ( have_posts() ): ?>

				<div class="posts">

					<?php while ( have_posts() ): the_post(); ?>

						<article>
							<div class="photo">
								<div class="content">
									<a href="<?php the_permalink(); ?>">
										<img src="<?php $image = get_field('hero_image'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
									</a>
								</div>
							</div>

							<div class="info">
								<div class="headline">
									<h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
								</div>

								<div class="meta">
									<p><?php the_time('n/j/y'); ?>: <a href="<?php $author = get_field('author'); echo get_permalink($author->ID); ?>"><?php echo get_the_title($author->ID); ?></a> | 	<?php $categories = get_the_category(); if ( ! empty( $categories ) ): ?><a href="<?php echo get_category_link($categories[0]->term_id); ?>"><?php echo $categories[0]->name; ?></a><?php endif; ?></p>
								</div>	

								<div class="cta">
									<a href="<?php the_permalink(); ?>" class="btn clear-charcoal">Read More</a>
								</div>	
							</div>
							
						</article>

					<?php endwhile; ?>



				</div>

			<?php endif; ?>

<?php the_posts_pagination( array(
	'mid_size'  => 2,
	'prev_text' => __(''),
	'next_text' => __(''),
) );

?>
		</div>
	</section>
	
<?php get_footer(); ?>