<?php

/*

    ----------------------------------------------------------------------
    					XX Theme Support
    ----------------------------------------------------------------------

*/

show_admin_bar( false );

function register_my_menu() {
  register_nav_menu('main-menu',__( 'Main Menu' ));
}
add_action( 'init', 'register_my_menu' );

function cc_mime_types($mimes) {
  $mimes['svg'] = 'image/svg+xml';
  return $mimes;
}
add_filter('upload_mimes', 'cc_mime_types');

function custom_tiled_gallery_width() {
    return '1200';
}
add_filter( 'tiled_gallery_content_width', 'custom_tiled_gallery_width' );

add_theme_support( 'title-tag' );
add_theme_support( 'html5', array( 'comment-list', 'comment-form', 'search-form', 'gallery', 'caption' ));
add_theme_support( 'custom-header', array(
    'video' => true
) );



/*

    ----------------------------------------------------------------------
    					XX Code Cleanup
    ----------------------------------------------------------------------

*/

remove_action( 'wp_head', 'wp_resource_hints', 2 );
remove_action ('wp_head', 'rsd_link');
remove_action( 'wp_head', 'wlwmanifest_link');
remove_action( 'wp_head', 'wp_shortlink_wp_head');
remove_action( 'wp_head', 'wp_generator');
remove_action( 'wp_head', 'rest_output_link_wp_head', 10 );
remove_action( 'wp_head', 'wp_oembed_add_discovery_links', 10 );
remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
remove_action( 'wp_print_styles', 'print_emoji_styles' );
remove_action( 'admin_print_styles', 'print_emoji_styles' );


function remove_image_size_attributes( $html ) {
    return preg_replace( '/(width|height)="\d*"/', '', $html );
}
 
// Remove image size attributes from post thumbnails
add_filter( 'post_thumbnail_html', 'remove_image_size_attributes' );
 
// Remove image size attributes from images added to a WordPress post
add_filter( 'image_send_to_editor', 'remove_image_size_attributes' );






/*

    ----------------------------------------------------------------------
    					XX Custom Functions
    ----------------------------------------------------------------------

*/



function remove_menus() {
    remove_menu_page( 'edit-comments.php' );
}

add_action( 'admin_menu', 'remove_menus' );






// Enqueue custom styles and scripts
function bearsmith_enqueue_styles_and_scripts() {
    // Register and noConflict jQuery 3.4.1
    wp_register_script( 'jquery.3.4.1', 'https://code.jquery.com/jquery-3.4.1.min.js' );
    wp_add_inline_script( 'jquery.3.4.1', 'var jQuery = $.noConflict(true);' );

    $uri = get_stylesheet_directory_uri();
    $dir = get_stylesheet_directory();

    $script_last_updated_at = filemtime($dir . '/js/site.js');
    $style_last_updated_at = filemtime($dir . '/style.css');

    // Add style.css and third-party css
    wp_enqueue_style( 'aos', 'https://unpkg.com/aos@next/dist/aos.css' );
    wp_enqueue_style( 'style', get_stylesheet_directory_uri() . '/style.css', '', $style_last_updated_at );

    // Add plugins.js & site.js (with jQuery dependency)
    wp_enqueue_script( 'custom-plugins', get_stylesheet_directory_uri() . '/js/plugins.js', array( 'jquery.3.4.1' ), $script_last_updated_at, true );
    wp_enqueue_script( 'custom-site', get_stylesheet_directory_uri() . '/js/site.js', array( 'jquery.3.4.1' ), $script_last_updated_at, true );
}
add_action( 'wp_enqueue_scripts', 'bearsmith_enqueue_styles_and_scripts' );



// LISTINGS FILTERS

//Enqueue Ajax Scripts
function enqueue_listings_filter_ajax_scripts() {
  wp_register_script('listings-filter', get_bloginfo('template_url') . '/js/listings-filter.js', '', true );
  wp_localize_script('listings-filter', 'ajax_listing_params', array( 'ajax_url' => admin_url( 'admin-ajax.php' ) ) );
  wp_enqueue_script('listings-filter', get_bloginfo('template_url') . '/js/listings-filter.js','','',true);
}
add_action('wp_enqueue_scripts', 'enqueue_listings_filter_ajax_scripts');

//Add Ajax Actions
add_action('wp_ajax_listings_filter', 'ajax_listings_filter');
add_action('wp_ajax_nopriv_listings_filter', 'ajax_listings_filter');


function ajax_listings_filter() {

    $query_data = $_GET;
    $query_array = array();

    if(isset($_GET['property_types']) && !empty($_GET['property_types'])) {

        $property_types = explode(',', $query_data['property_types']);

        array_push($query_array, 
            array(
                'key' => 'property_type',
                'value' => $property_types,
                'compare' => 'IN'
            ) 
        );
    }

    if(isset($_GET['property_status']) && !empty($_GET['property_status'])) {

        $status = $query_data['property_status'];

        if($status == 'on-the-market') {
            $status = 'a_on-the-market';
        } elseif($status == 'coming-soon') {
            $status = 'b_coming-soon';
        } elseif($status == 'under-contract') {
            $status = 'c_under-contract';
        } elseif($status == 'sold') {
            $status = 'd_sold';
        }

        array_push($query_array, 
            array(
                'key' => 'status',
                'value' => $status,
                'compare' => '='
            ) 
        );
    }

    $listings_args = array(
        'post_type' => 'listings',
        'posts_per_page' => 200,
        'paged' => $paged,
        'post_status' => 'publish',
        'meta_key' => 'status',
        'orderby'    => array(
            'meta_value' => 'ASC',
            'date' => 'DESC'
        ),
        'meta_query' =>  $query_array
    );

    $listings_loop = new WP_Query($listings_args);

    if($_GET['property_types'] !== null) : 
        if( $listings_loop->have_posts() ): 
            while( $listings_loop->have_posts() ): $listings_loop->the_post();
                get_template_part('partials/listing');
            endwhile;   
        else:
            get_template_part('partials/listing-none');
        endif;
    else: 
        get_template_part('partials/listing-none');
    endif;

    wp_reset_postdata();

    die();
}



// MAP LISTINGS FILTERS

//Enqueue Ajax Scripts
function enqueue_map_listings_filter_ajax_scripts() {
  wp_register_script('map-filter', get_bloginfo('template_url') . '/js/map-filter.js', '', true );
  wp_localize_script('map-filter', 'ajax_map_listings_params', array( 'ajax_url' => admin_url( 'admin-ajax.php' ) ) );
  wp_enqueue_script('map-filter', get_bloginfo('template_url') . '/js/map-filter.js','','',true);
}
add_action('wp_enqueue_scripts', 'enqueue_map_listings_filter_ajax_scripts');

//Add Ajax Actions
add_action('wp_ajax_map_listings_filter', 'ajax_map_listings_filter');
add_action('wp_ajax_nopriv_map_listings_filter', 'ajax_map_listings_filter');

function ajax_map_listings_filter() {

    get_template_part('partials/map/ajax-map-view');

    wp_reset_postdata();

    die();
}



/*

    ----------------------------------------------------------------------
    					XX Advanced Custom Fields
    ----------------------------------------------------------------------

*/


if( function_exists('acf_add_options_page') ) {
    acf_add_options_page('Site Options');
}

function my_relationship_query( $args, $field, $post_id ) {
    $args['orderby'] = 'date';
    $args['order'] = 'DESC';
    return $args;
}
// filter for every field
add_filter('acf/fields/relationship/query', 'my_relationship_query', 10, 3);

function my_acf_admin_head() {
	?>
	<style type="text/css">

		.acf-relationship .list {
			height: 400px;
		}

	</style>
	<?php
}
add_action('acf/input/admin_head', 'my_acf_admin_head');


function my_acf_init() {
    $google_maps_api_key = get_field('google_maps_api_key', 'options');
    acf_update_setting('google_api_key', $google_maps_api_key);
}
add_action('acf/init', 'my_acf_init');


// SEO Images
add_filter('wpseo_opengraph_image', 'og_image');
add_filter('wpseo_twitter_image', 'og_image');
function og_image($image) {
    global $post;
 
    if (get_field('hero_image', $post->ID)) {
        $image_obj = get_field('hero_image', $post->ID);
        $image = $image_obj['url'];
    }
    return $image;
}