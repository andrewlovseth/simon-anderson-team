	<footer class="cover" style="background-image: url(<?php $image = get_field('footer_background_image', 'options'); echo $image['url']; ?>);">

		<div class="newsletter" id="subscribe">
			<div class="wrapper">
				<div class="headline">
					<h3><?php the_field('newsletter_headline', 'options'); ?></h3>
				</div>

				<div class="form-wrapper">
					<?php
						$shortcode = get_field('newsletter_form_embed', 'options');
						echo do_shortcode($shortcode);
					?>					
				</div>
			</div>
		</div>

		<div class="footer-links">
			<div class="wrapper">
					
				<?php if(have_rows('footer_navigation', 'options')): while(have_rows('footer_navigation', 'options')) : the_row(); ?>
	 			    <?php if( get_row_layout() == 'column' ): ?>
						
						<div class="col">
							<div class="main-link">
								<?php 

								$link = get_sub_field('main_link');

								if( $link ): 
									$link_url = $link['url'];
									$link_title = $link['title'];
									$link_target = $link['target'] ? $link['target'] : '_self';
									?>
									<a href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr( $link_target ); ?>"><?php echo esc_html($link_title); ?></a>
								<?php endif; ?>
							</div>

							<div class="sub-links">
								<?php if(have_rows('links')): while(have_rows('links')): the_row(); ?>
								 
									<?php 

									$link = get_sub_field('link');

									if( $link ): 
										$link_url = $link['url'];
										$link_title = $link['title'];
										$link_target = $link['target'] ? $link['target'] : '_self';
										?>
										<a href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr( $link_target ); ?>"><?php echo esc_html($link_title); ?></a>
									<?php endif; ?>

								<?php endwhile; endif; ?>
							</div>
				    		<?php the_sub_field('text'); ?>
						</div>
						
				    <?php endif; ?>			 
				<?php endwhile; endif; ?>

			</div>
		</div>

		<div class="contact-info">
			<div class="wrapper">

				<div class="corporate">
					<?php the_field('corporate_name', 'options'); ?>
				</div>

				<div class="corporate2">
					<?php the_field('address', 'options'); ?>
				</div>

				<div class="phone item">
					<a href="tel:<?php the_field('phone', 'options'); ?>"><?php the_field('phone', 'options'); ?></a> | <a href="mailto:<?php the_field('email', 'options'); ?>"><?php the_field('email', 'options'); ?></a> | <?php 
							$link = get_field('corporate_link', 'options');
							if( $link ): 
							    $link_url = $link['url'];
							    $link_title = $link['title'];
							    $link_target = $link['target'] ? $link['target'] : '_self';
							    ?>
							    <a href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>"><?php echo esc_html( $link_title ); ?></a>
							<?php endif; ?>
				</div>


			</div>
		</div>

		<div class="legal">
			<div class="wrapper">
				<span><?php the_field('copyright', 'options'); ?></span>

				<a href="<?php echo site_url('/terms-and-conditions/'); ?>">Terms & Conditions</a>

				<a href="<?php echo site_url('/privacy-policy/'); ?>">Privacy Policy</a>
			</div>
		</div>

	</footer>


	<div id="overlay">
		<div id="overlay-wrapper">
			
		</div>
	</div>

	<?php get_template_part('template-parts/header/search-form-desktop'); ?>

	<?php wp_footer(); ?>

</body>
</html>